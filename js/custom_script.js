function inputChecked(elem) {
	var love = document.getElementById('love');
	var friend = document.getElementById('friend');
	var money = document.getElementById('money');

	switch (elem) {
		case 'love':
			love.classList.toggle('active');
			if (friend.firstElementChild.checked && money.firstElementChild.checked) {
				document.getElementById('friend').firstElementChild.checked = false;
				friend.classList.toggle('active');
			}
			break;
		case 'friend':
			friend.classList.toggle('active');
			if (love.firstElementChild.checked && money.firstElementChild.checked) {
				document.getElementById('money').firstElementChild.checked = false;
				money.classList.toggle('active');
			}
			break;
		case 'money':
			money.classList.toggle('active');
			if (love.firstElementChild.checked && friend.firstElementChild.checked) {
				document.getElementById('love').firstElementChild.checked = false;
				love.classList.toggle('active');
			}
			break;
		default:
			alert("Something went wrong");
	}
}